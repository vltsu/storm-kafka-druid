package com.kafkastorm.example.subscriber;

import backtype.storm.task.IMetricsContext;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.metamx.common.Granularity;
import com.metamx.tranquility.beam.Beam;
import com.metamx.tranquility.beam.ClusteredBeamTuning;
import com.metamx.tranquility.druid.DruidBeams;
import com.metamx.tranquility.druid.DruidLocation;
import com.metamx.tranquility.druid.DruidRollup;
import com.metamx.tranquility.storm.BeamFactory;
import com.metamx.tranquility.typeclass.Timestamper;
import io.druid.granularity.QueryGranularity;
import io.druid.query.aggregation.AggregatorFactory;
import io.druid.query.aggregation.CountAggregatorFactory;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.BoundedExponentialBackoffRetry;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.text.DateFormat;
import java.util.List;
import java.util.Map;
import java.util.logging.SimpleFormatter;

public class DruidBeamFactory implements BeamFactory<Map<String, Object>> {
    private static final String ZOOKEEPER_CONNECTION_STRING = "127.0.0.1:2282";
    private static final int BASE_SLEEP_TIME = 100,
            MAX_SLEEP_TIME_MILLIS = 1000,
            MAX_RETRIES = 5;

    public Beam<Map<String, Object>> makeBeam(Map<?, ?> map, IMetricsContext imc) {
        try {
            final CuratorFramework curator = CuratorFrameworkFactory.newClient(ZOOKEEPER_CONNECTION_STRING,
                    new BoundedExponentialBackoffRetry(
                            BASE_SLEEP_TIME,
                            MAX_SLEEP_TIME_MILLIS,
                            MAX_RETRIES)
            );
            curator.start();
            // TODO: Set the dimensions correctly
            final List<String> dimensions = ImmutableList.of("url", "params");
            final String dataSource = "test-topic",
                    indexService = "overlord",
                    fireHosePattern = "druid:firehose:%s",
                    discoveryPath = "/druid/discovery";

            final List<AggregatorFactory> aggregators = ImmutableList.<AggregatorFactory>of(
                    new CountAggregatorFactory(
                            "cnt"
                    )
            );

            final Timestamper<Map<String, Object>> timestamper = new Timestamper<Map<String, Object>>() {
                @Override
                public DateTime timestamp(Map<String, Object> theMap) {
                    return new DateTime(theMap.get("timestamp"));
                }
            };

            final DruidBeams.Builder<Map<String, Object>> builder = DruidBeams
                    .builder(timestamper)
                    .curator(curator)
                    .discoveryPath(discoveryPath)
                    .location(
                            DruidLocation.create(
                                    indexService, fireHosePattern, dataSource
                            )
                    )
                    .rollup(DruidRollup.create(dimensions, aggregators, QueryGranularity.MINUTE))
                            // Duration of 10months from current...
                    .tuning(ClusteredBeamTuning.create(Granularity.HOUR, new Period("PT0M"), new Period("PT10M"), 1, 1));
            return builder.buildBeam();
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }
}